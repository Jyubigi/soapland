#pragma once

#include "stdafx.h"

class MiniScene : public AT::GameScene<MiniScene>
{
private:
	//!< 애니메이션
	GH::CGHAnimation * m_pAni;
	GH::CGHAnimation * m_pAni2;
	ingame			 * m_ingame;
	Enemy			 * m_Enemy[D_ENEMY_MAX];
	float			   gametime;
	int				   createcount;
	int				   num;
	int				   highscore;
	int				   d_high;
	int				   touchcount;
	Point			   m_cPos;
	CCLabelTTF *pLabel;
	CCLabelTTF *Highscore;
	char coinScore[100];
	int score;

	JellyTip * tip;

public:
	//!< 씬 생성에 대한 싱글톤
	//!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
	MiniScene();

	virtual bool init();

	//!< 콜백 함수 (이 씬에선 종료할 때 쓰임)
	void menuCloseCallback(Ref* pSender);
	void Update(float dt);
	bool RectVsRectCollision(Rect rect1, Rect rect2);
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onTouchMoved(Touch* touch, Event* unused_event);
	virtual void onTouchCancelled(Touch* touch, Event* unused_event);
	virtual void onTouchEnded(Touch* touch, Event *unused_event);

	//!< 씬을 등록 시킴
	CREATE_FUNC(MiniScene);
};