#pragma once

class AromaTip : public Layer
{
private:
	int touchcount = 0;
	GH::CGHSprite * back[2];
public:
	virtual bool init();
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onEnterTransitionDidFinish()
	{
		auto listener = EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);

		listener->onTouchBegan = CC_CALLBACK_2(AromaTip::onTouchBegan, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
	CREATE_SCENE_FUNC(AromaTip);
	CREATE_FUNC(AromaTip);
};