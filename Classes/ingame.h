#pragma once

#include"stdafx.h"

class ingame
{
private:

	GH::CGHAnimation * m_pAni;
	Point m_cPos;
	Rect rect_1;
	int	 num;
	bool state;
	bool m_bJumpOn;
public:

	void init(Layer *	pLayer);
	void update(float dt);
	void pos(int pos1);
	void setJump(float fJumpForce);
	Point getPos();
	Rect getRect();
	ingame(){}
	~ingame(){}

};