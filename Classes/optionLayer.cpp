#include "stdafx.h"

void optionLayer::Init(Layer * pl)
{
	m_pl = pl;
	_eventDispatcher->pauseEventListenersForTarget(pl);
	//auto _speaker = MenuItemImage::create("UI/Sound.png", "UI/Sound1.png", CC_CALLBACK_1(chgame::game1, this));

	auto _ex = MenuItemImage::create("UI/Back.png", "UI/Back1.png", CC_CALLBACK_1(optionLayer::exitCallBack, this));
	_ex->setPosition(ccp(D_DESIGN_WIDTH / 2 + 180, D_DESIGN_HEIGHT / 2+80));
	auto exm = Menu::create(_ex, NULL);
	exm->setPosition(Vec2::ZERO);
	this->addChild(exm, 5);

	_speaker = new GH::CGHSprite;
	_speaker->initWithFile("UI/Sound.png");
	_speaker->setPosition(ccp(D_DESIGN_WIDTH / 2 - 40, D_DESIGN_HEIGHT / 2));
	//auto speakerm = Menu::create(_speaker, NULL);
	this->addChild(_speaker, 5);

	_dispeaker = new GH::CGHSprite;
	_dispeaker->initWithFile("UI/Sound1.png");
	_dispeaker->setPosition(ccp(D_DESIGN_WIDTH / 2 - 40, D_DESIGN_HEIGHT / 2));
	//auto speakerm = Menu::create(_speaker, NULL);
	this->addChild(_dispeaker, 5);
	_dispeaker->setVisible(false);

	auto _bg = new GH::CGHSprite;
	_bg->initWithFile("UI/popup1.png");
	_bg->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	//auto speakerm = Menu::create(_speaker, NULL);
	this->addChild(_bg, 1);

	auto _plus = MenuItemImage::create("UI/plus.png", "UI/plus1.png", CC_CALLBACK_1(optionLayer::plusCallBack, this));
	_plus->setPosition(ccp(D_DESIGN_WIDTH / 2 + 130, D_DESIGN_HEIGHT / 2));
	auto plusm = Menu::create(_plus, NULL);
	plusm->setPosition(Vec2::ZERO);
	this->addChild(plusm, 5);

	auto _minus = MenuItemImage::create("UI/minus.png", "UI/minus1.png", CC_CALLBACK_1(optionLayer::minusCallBack, this));
	_minus->setPosition(ccp(D_DESIGN_WIDTH / 2 - 130, D_DESIGN_HEIGHT / 2));
	auto minusm = Menu::create(_minus, NULL);
	minusm->setPosition(Vec2::ZERO);
	this->addChild(minusm, 5);

	auto vol = UserDefault::getInstance()->getIntegerForKey("Volume");
	m_pLabel = LabelTTF::create();
	char buf[60];
	sprintf(buf, "%d", (int)vol);
	m_pLabel->initWithString(buf, "Arial", 100);
	m_pLabel->setPosition(ccp(D_DESIGN_WIDTH / 2+45, D_DESIGN_HEIGHT / 2));
	this->addChild(m_pLabel,5);

	m_nVol = UserDefault::getInstance()->getIntegerForKey("Volume");//!< 시작시 저장해둔 볼륨을 불러온다
	applyVol();

	auto _listener = EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);

	_listener->onTouchBegan = CC_CALLBACK_2(optionLayer::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
}

void optionLayer::applyVol()//!< 볼륨 값을 적용한다
{
	if (m_nVol <= 0)
	{
		m_nVol = 0;
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		_dispeaker->setVisible(true);
		_speaker->setVisible(false);
	}
	else if (m_nVol == 1)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		_dispeaker->setVisible(false);
		_speaker->setVisible(true);
	}
	else if (m_nVol >= 8)
	{
		m_nVol = 8;
	}
	CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume((float)m_nVol);
	char buf[60];
	sprintf(buf, "%d", m_nVol);
	m_pLabel->setString(buf);
}

bool optionLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
	if (_speaker->getBoundingBox().containsPoint(touch->getLocation()))
	{
		if (m_nVol <= 0)
		{
			m_nVol = 1;
		}
		else
		{
			m_nVol = 0;
		}
		applyVol();
	}
	return true;
}

void optionLayer::minusCallBack(Ref * pSander)
{
	m_nVol -=1;
	applyVol();
}
void optionLayer::plusCallBack(Ref * pSander)
{
	m_nVol += 1;
	applyVol();
}
void optionLayer::exitCallBack(Ref * pSander)
{
	Destroy();
}
void optionLayer::Destroy()
{
	UserDefault::getInstance()->setIntegerForKey("Volume", m_nVol);//!< 종료 시 볼륨 값을 저장한다.
	_eventDispatcher->resumeEventListenersForTarget(this);
	removeAllChildren();
	removeFromParentAndCleanup(true);
}