#pragma once

namespace AC
{
	class BackLayer : public cocos2d::Layer, public TempleteSingleton<BackLayer>
	{
	private:
		GH::CGHSprite * backSprite ;
		bool m_bInit;
		Layer* m_pLayer;

	public:
		BackLayer()
		{
			backSprite = nullptr;
			m_bInit = false;
		}
		~BackLayer(){}
	public:
		bool init(Layer * pLayer);
		bool Destroy();

		void BackButtonCallback(Ref* pSender);
		void yesButtonCallback(Ref* pSender);
		//void HomeButtonCallback(Ref* pSender);

		virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	};
}