#include "stdafx.h"

void creditLayer::init(Layer * pl)
{
	m_pLayer = pl;

	_eventDispatcher->pauseEventListenersForTarget(pl);
	auto image = new GH::CGHSprite;
	image->initWithFile("credit.png");
	image->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(image);

	auto _ex = MenuItemImage::create("UI/x.png", "UI/x.png", CC_CALLBACK_1(creditLayer::exit, this));
	_ex->setPosition(ccp(D_DESIGN_WIDTH / 2 + 170, D_DESIGN_HEIGHT / 2 + 150));
	auto exm = Menu::create(_ex, NULL);
	exm->setPosition(Vec2::ZERO);
	this->addChild(exm, 5);

	auto _listener = EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);

	_listener->onTouchBegan = CC_CALLBACK_2(creditLayer::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
}

bool creditLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
	return true;
}
void creditLayer::exit(Ref * pSander)
{
	_eventDispatcher->resumeEventListenersForTarget(m_pLayer);
	removeAllChildren();
	removeFromParentAndCleanup(true);
}