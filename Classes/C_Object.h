#pragma once

namespace A
{
	class Object
	{
	protected:
		GH::CGHSprite * m_pSprite = nullptr;//!< 스프라이트 벡터

		Layer* m_pLayer;

	public:
		virtual void init(Layer * pLayer);
		virtual void Destroy();
		GH::CGHSprite * getSprite();
	};
}