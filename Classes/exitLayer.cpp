#include "stdafx.h"

bool AC::exitLayer::Init(Layer * pLayer)
{
	backSprite = new GH::CGHSprite;
	if (!backSprite->initWithFile("UI/Popup.png"))
		return false;

	_eventDispatcher->pauseEventListenersForTarget(m_pscene);
	
	backSprite->setPosition(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2);
	this->addChild(backSprite,0);
	m_pscene = pLayer;

	auto m1 = MenuItemImage::create("UI/yes.png", "UI/yes.png", CC_CALLBACK_1(exitLayer::closeCallback, this));
	m1->setPosition(ccp(D_DESIGN_WIDTH / 2 - 80, D_DESIGN_HEIGHT / 2 - 20));
	m1->setZOrder(4);
	auto mm1 = Menu::create(m1, NULL);
	mm1->setPosition(Vec2::ZERO);
	mm1->setZOrder(4);
	this->addChild(mm1, 5);

	auto m2 = MenuItemImage::create("UI/no.png", "UI/no.png", CC_CALLBACK_1(exitLayer::noCallback, this));
	m2->setPosition(ccp(D_DESIGN_WIDTH / 2 + 80, D_DESIGN_HEIGHT / 2 - 20));
	m2->setZOrder(4);
	auto mm2 = Menu::create(m2, NULL);
	mm2->setPosition(Vec2::ZERO);
	mm2->setZOrder(4);
	this->addChild(mm2, 5);

	pLayer->addChild(this, 11);

	this->setKeypadEnabled(true);

	auto _listener = EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);
	_listener->onTouchBegan = CC_CALLBACK_2(exitLayer::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);

	return true;
}
bool AC::exitLayer::Destroy()
{
	this->removeAllChildren();
	CC_SAFE_DELETE(backSprite);
	this->removeFromParentAndCleanup(true);
	_eventDispatcher->resumeEventListenersForTarget(m_pscene);
	return true;
}

bool AC::exitLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
	return true;
}
void AC::exitLayer::noCallback(Ref * pSander)
{
	Destroy();
}
void AC::exitLayer::closeCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	//!< 메시지 박스 출력 하고 반환
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	//!< 시퀸스 종료
	Director::getInstance()->end();

	//!< ios 일 경우
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	//!< 종료
	exit(0);
#endif
}