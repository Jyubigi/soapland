#pragma once

#include "stdafx.h"

class logo : public LayerColor
{
private:
	GH::CGHAnimation *		m_pAni;
	bool m_bNext;

public:
	//!< 씬 생성에 대한 싱글톤
	static Scene* createScene();

	//!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
	virtual bool init();

	void next();
	//!< 콜백 함수 (이 씬에선 종료할 때 쓰임)

	void Update(float dt);

	//!< 씬을 등록 시킴
	CREATE_FUNC(logo);


};