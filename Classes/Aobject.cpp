#include "stdafx.h"

bool Aobject::Init(Layer * pLayer, Vec2 ind, int objectNum)
{
	m_pAni = new GH::CGHSprite;

	char buf[64];
	sprintf(buf, "ob%d.png", objectNum);
	if (!m_pAni->initWithFile(buf))
		return false;
	m_direction = objectNum%2;
	pLayer->addChild(m_pAni, 10);

	m_cPos = ind;
	m_size = objectNum/2;
	Move(m_cPos);

	return true;
}
void Aobject::Destroy()
{
	m_pAni->removeFromParentAndCleanup(true);
	CC_SAFE_DELETE(m_pAni);
}

void Aobject::Move(Vec2 mpos)
{
	//m_cPrPos = m_cPos;
	//m_cPos = mpos;
	if (m_direction) // Ȧ
	m_pAni->setPosition(ccp(BACKPOSX + (mpos.x * TILESIZE) + TILESIZE / 2
	, BACKPOSY + BACKHIGHT - ((mpos.y + (int)m_size / 2) *TILESIZE + (TILESIZE / 2 * (m_size % 2)))));
	else // ¦
		m_pAni->setPosition(ccp(BACKPOSX + (mpos.x + (int)m_size / 2)*TILESIZE + (TILESIZE / 2 * (m_size % 2))
		, BACKPOSY + BACKHIGHT - (mpos.y *TILESIZE + TILESIZE / 2)));
}

void Aobject::MoveCancel()
{
	//m_cPos = m_cPrPos;
	if (m_direction) // Ȧ
		m_pAni->setPosition(ccp(BACKPOSX + m_cPrPos.x*TILESIZE + TILESIZE / 2
		, BACKPOSY + BACKHIGHT - (m_cPrPos.y *TILESIZE + TILESIZE / 2 - TILESIZE *(m_size - 1))));
	else // ¦
		m_pAni->setPosition(ccp(BACKPOSX + m_cPrPos.x*TILESIZE + TILESIZE / 2 - TILESIZE *(m_size - 1)
		, BACKPOSY + BACKHIGHT - (m_cPrPos.y *TILESIZE + TILESIZE / 2)));
}

Vec2 Aobject::getIndex()
{
	return m_cPos;
}
void Aobject::setIndex(Vec2 ind)
{
	m_cPos.x = ind.x;
	m_cPos.y = ind.y;
}
void Aobject::setPos(Point pos){
	m_pAni->setPosition(pos);
}
Sprite * Aobject::getSprite()
{
	return m_pAni;
}
int Aobject::getsize()
{
	return m_size;
}
bool Aobject::getdirection()
{
	return m_direction;
}