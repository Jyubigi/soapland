#include "stdafx.h"

ResultLayer::ResultLayer()
{
	
}
bool ResultLayer::init()
{
	if (!Layer::init())
		return false;

	CCDirector::sharedDirector()->pause();

	m_pBack = new GH::CGHSprite;
	m_pBack->initWithFile("UI/BG.png");
	m_pBack->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(m_pBack, 1);

	int score = (int)A::ScoreCounter::getInstance()->getScore();
	int highscore = (int)A::ScoreCounter::getInstance()->getBestScore();
	m_eGame = A::ScoreCounter::getInstance()->getGameNum();

	m_pResult = new GH::CGHSprite;
	m_pResult->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));

	char labelbuf[256];
	sprintf(labelbuf, "%d", score);
	Point pos;


	switch (m_eGame)
	{
	case E_GAME_NUM::RUSH:
		m_pResult->initWithFile("rushre.png");
		sprintf(labelbuf, "%d", score);
		pos = ccp(D_DESIGN_WIDTH - 200, D_DESIGN_HEIGHT - 70);
		break;
	case E_GAME_NUM::AROMA:
		m_pResult->initWithFile("aromare.png");
		sprintf(labelbuf, "%d", score);
		pos = ccp(D_DESIGN_WIDTH - 180, D_DESIGN_HEIGHT - 70);
		break;
	case E_GAME_NUM::JELLY:
		m_pResult->initWithFile("jellyre.png");
		sprintf(labelbuf, "%d m", score);
		pos = ccp(D_DESIGN_WIDTH - 170, D_DESIGN_HEIGHT - 70);
		break;
	case E_GAME_NUM::YOKAN:
		m_pResult->initWithFile("yokanre.png");
		sprintf(labelbuf, "%d cm", score);
		pos = ccp(D_DESIGN_WIDTH - 110, D_DESIGN_HEIGHT - 130);
		break;
	default:
		break;
	}
	this->addChild(m_pResult, 3);

	auto label = LabelTTF::create(labelbuf, "Arial", 50);
	label->setPosition(pos);
	this->addChild(label, 10);

	auto m1 = MenuItemImage::create("UI/exit.png", "UI/exit1.png", CC_CALLBACK_1(ResultLayer::Exit, this));
	m1->setPosition(ccp(D_DESIGN_WIDTH / 2 +100, D_DESIGN_HEIGHT / 4 * 2 + 100));
	m1->setZOrder(4);
	auto mm1 = Menu::create(m1, NULL);
	mm1->setPosition(Vec2::ZERO);
	mm1->setZOrder(4);
	this->addChild(mm1, 5);

	auto m2 = MenuItemImage::create("UI/retry.png", "UI/retry1.png", CC_CALLBACK_1(ResultLayer::Again, this));
	m2->setPosition(ccp(D_DESIGN_WIDTH / 2 - 100, D_DESIGN_HEIGHT / 4 * 2 + 100));
	m2->setZOrder(4);
	auto mm2 = Menu::create(m2, NULL);
	mm2->setPosition(Vec2::ZERO);
	mm2->setZOrder(4);
	this->addChild(mm2, 5);

	//m_pAgain = LabelTTF::create("Again", "Arial", 50);
	//m_pAgain->setPosition(ccp(D_DESIGN_WIDTH / 2 - 100, D_DESIGN_HEIGHT / 4 * 2 +100));
	//this->addChild(m_pAgain, 10);

	char best[256];
	sprintf(best, "%d", highscore);
	auto blabel = LabelTTF::create(best, "Arial", 100);
	blabel->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 4 + 50));
	this->addChild(blabel, 10);

	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(ResultLayer::onTouchBegan, this);
	
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void ResultLayer::Destroy()
{
	removeAllChildren();
	removeFromParentAndCleanup(true);
	CC_SAFE_DELETE(m_pBack);
	CC_SAFE_DELETE(m_pResult);
}
bool ResultLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
	return true;
}

void ResultLayer::Exit(Ref * pSander)
{
	removeAllChildren();
	removeFromParentAndCleanup(true);
	Scene * next = chgame::createScene();
	Director::getInstance()->replaceScene(next);
}
void ResultLayer::Again(Ref * pSander)
{
	switch (m_eGame)
	{
	case E_GAME_NUM::RUSH:
		Director::getInstance()->replaceScene(RushScene::createScene());
		break;
	case E_GAME_NUM::AROMA:
		Director::getInstance()->replaceScene(Mini2Scene::createScene());
		break;
	case E_GAME_NUM::JELLY:
		Director::getInstance()->replaceScene(MiniScene::createScene());
		break;
	case E_GAME_NUM::YOKAN:
		Director::getInstance()->replaceScene(YokanScene::createScene());
		break;
	default:
		break;
	}
}