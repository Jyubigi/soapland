#pragma once

template <typename T>

class TempleteSingleton
{
protected:
	TempleteSingleton(){}
public:
	virtual ~TempleteSingleton(){}
	static T * getInstance()
	{
		static T * ins = new T;
		return ins;
	}
};
