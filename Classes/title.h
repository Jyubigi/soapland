#pragma once

class title : public Layer
{
private:
	LabelTTF * m_pLabel;
	//int m_nLoadedSpriteCount = 0;
	//int m_nSpriteCount =15;
public:
	title();

	virtual bool init();
	virtual void Update(float dt);
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);

	void loadingCallBack(Object *obj);

	CREATE_FUNC(title);
	CREATE_SCENE_FUNC(title);
};