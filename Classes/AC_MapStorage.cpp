#include "stdafx.h"

void AC::MapStorage::init()
{
	ssize_t 	    bufferSize;
	for (int i = 1; i <= 7; i++)
	{
		char buf[64];
		sprintf(buf, "map%d.txt", i);
		m_pMaps[i] = (char*)FileUtils::getInstance()->getFileData(buf, "rt", &bufferSize);
	}
}
const char* AC::MapStorage::getMapAt(int at)
{
	return m_pMaps[at].c_str();
}