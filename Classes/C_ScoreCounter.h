#pragma once

namespace A
{
	class ScoreCounter : public TempleteSingleton<ScoreCounter>
	{
	private:
		int m_nScore =0;
		int m_nBest = 0;
		float m_fTime = 0.0f;
		E_GAME_NUM m_eGameNum = E_GAME_NUM::AROMA;
		std::string m_strInkey;

	public:
		ScoreCounter(){}
		~ScoreCounter(){}

	public:
		void init(E_GAME_NUM gamenum);
		void update(float dt);
		void takeScore(int sco);

		E_GAME_NUM getGameNum();

		void timeIsScore();
		int getBestScore()const;
		int getScore();
		float getTime() const;
	};
}