#pragma once

enum class E_DIRECTION : unsigned int
{
	NONE = 0, UP, DOWN, RIGHT,LEFT
};

namespace A
{
	class Swyper : public TempleteSingleton<Swyper>
	{
	protected:
		Point m_cInitPos = ccp(0,0);
		Point m_cCurPos = ccp(0,0);

	public:
		Swyper(){}
		virtual ~Swyper(){}

		void onTouchBegun(Point tpos);
		void onTouchEnded(Point tpos);

		E_DIRECTION getDirection();
	};
}