#pragma once

class YokanScene : public AT::GameScene<YokanScene>
{
private:
	A::Hero * m_pHero;

public:

	//!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
	virtual bool init();

	//!< 콜백 함수 (이 씬에선 종료할 때 쓰임)

	void Update(float dt);
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onTouchMoved(Touch* touch, Event* unused_event);
	virtual void onTouchCancelled(Touch* touch, Event* unused_event);
	virtual void onTouchEnded(Touch* touch, Event *unused_event);

	//!< 씬을 등록 시킴
	CREATE_FUNC(YokanScene);
};