#pragma once
#include "stdafx.h"

namespace AT
{
	template <typename SceneName>//!< GameScene<�� �̸�>
	class GameScene : public CCLayerColor
	{
	protected:
		AC::BackLayer * bl;
		EventListenerTouchOneByOne * listener;
		bool m_bOver = false;


	public:
		static Scene * createScene();

		virtual bool init(){ return true; bl = nullptr; }
		virtual void Update(float dt){}
		virtual void Destroy();
		virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event);

		virtual void gemeOver();
		//!< ���� ��� ��Ŵ
		CREATE_FUNC(SceneName);
	};
}
template <typename SceneName>
Scene * AT::GameScene<SceneName>::createScene()
{
	log("log : game scene created....");
	auto scene = Scene::create();
	auto layer = SceneName::create();
	scene->addChild(layer);
	return scene;
}

template <typename SceneName>
void AT::GameScene<SceneName>::onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event)
{
	if (!AC::BackLayer::getInstance()->init(this))
	{
		AC::BackLayer::getInstance()->Destroy();
	}
}
template <typename SceneName>
void AT::GameScene<SceneName>::Destroy()
{
	if (bl != nullptr)
	{
		bl->Destroy();
		this->removeChild(bl);
		CC_SAFE_DELETE(bl);
	}
}

template <typename SceneName>
void AT::GameScene<SceneName>::gemeOver()
{
	if (m_bOver)
		return;
	this->setKeypadEnabled(false);
	m_bOver = true;
	Layer * re = ResultLayer::create();
	this->addChild(re, 15);
	_eventDispatcher->removeEventListener(listener);
}