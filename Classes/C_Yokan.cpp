#include "stdafx.h"

void A::Yokan::init(Layer * pLayer, E_YOKAN type)
{
	m_pSprite = new GH::CGHSprite;
	m_pLayer = pLayer;
	m_eType = type;
	m_nPoint = (int)m_eType * 50 + 50;
	char name[256];
	sprintf(name, "yokan%d.png", (int)m_eType);
	m_pSprite->initWithFile(name);
	pLayer->addChild(m_pSprite);
	m_fSpeed = 120.8f;
}
void A::Yokan::Update(float dt)
{
	m_nPoint = (int)m_eType * 50 + 50;
}

void A::Yokan::setType(E_YOKAN yotype)
{
	m_eType = yotype;
}

float A::Yokan::getSpeed() const
{
	return m_fSpeed;
}
void A::Yokan::setSpeed(float speed)
{
	m_fSpeed = speed;
}
int A::Yokan::getPoint() const
{
	return m_nPoint;
}