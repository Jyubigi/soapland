#pragma once
#include "cocos2d.h"

USING_NS_CC;

#define ReplaceScene(X) (Director::getInstance()->replaceScene(Loading<X>::createScene()))
#define PushScene(X) (Director::getInstance()->pushScene(Loading<X>::createScene()))


template<typename T>
class Loading : public Layer
{
private:
	Loading()
	{

	}
	virtual void onEnterTransitionDidFinish()
	{
		Layer::onEnterTransitionDidFinish();
		Scene * next = T::createScene();
		Director::getInstance()->replaceScene(next);
	}

public:
	CREATE_SCENE_FUNC(Loading);
	// implement the "static node()" method manually
	CREATE_FUNC(Loading);
}; 