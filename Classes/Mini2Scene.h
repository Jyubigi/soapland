#pragma once

#include "stdafx.h"

class Mini2Scene : public AT::GameScene<Mini2Scene>
{
private:
	//!< 애니메이션
	hero			 * m_hero;
	Monster			 * m_Enemy[D_ENEMY_MAX];
	float			   gametime;
	int				   createcount;

public:
	//!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
	virtual bool init();

	void Update(float dt);
	void left(Ref* pSender);
	void right(Ref* pSender);

	bool RectVsRectCollision(Point target1, Point target2, Rect rect1, Rect rect2);
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onTouchMoved(Touch* touch, Event* unused_event);
	virtual void onTouchCancelled(Touch* touch, Event* unused_event);
	virtual void onTouchEnded(Touch* touch, Event *unused_event);
	//!< 씬을 등록 시킴
	CREATE_FUNC(Mini2Scene);
};