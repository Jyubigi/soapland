#include "stdafx.h"
using namespace AC;

bool BackLayer::init(Layer * pLayer)
{
	if (m_bInit == true)
		return false;
	m_bInit = true;
	backSprite = new GH::CGHSprite;
	m_pLayer = pLayer;

	if (!backSprite->initWithFile("UI/Popup.png"))
		return false;
	backSprite->setPosition(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2);
	this->addChild(backSprite, 5);

	auto m1 = MenuItemImage::create("UI/yes.png", "UI/yes.png", CC_CALLBACK_1(AC::BackLayer::yesButtonCallback, this));
	m1->setPosition(ccp(D_DESIGN_WIDTH / 2 - 80, D_DESIGN_HEIGHT / 2 - 20));
	m1->setZOrder(4);
	auto mm1 = Menu::create(m1, NULL);
	mm1->setPosition(Vec2::ZERO);
	mm1->setZOrder(4);
	this->addChild(mm1, 5);

	auto m2 = MenuItemImage::create("UI/no.png", "UI/no.png", CC_CALLBACK_1(AC::BackLayer::BackButtonCallback, this));
	m2->setPosition(ccp(D_DESIGN_WIDTH / 2 + 80, D_DESIGN_HEIGHT / 2 - 20));
	m2->setZOrder(4);
	auto mm2 = Menu::create(m2, NULL);
	mm2->setPosition(Vec2::ZERO);
	mm2->setZOrder(4);
	this->addChild(mm2, 5);

	pLayer->addChild(this,10);

	_eventDispatcher->pauseEventListenersForTarget(m_pLayer);

	CCDirector::sharedDirector()->pause();
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	return true;
}
bool BackLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
	return true;
}

bool BackLayer::Destroy()
{
	CCDirector::sharedDirector()->resume();
	_eventDispatcher->resumeEventListenersForTarget(m_pLayer);

	if (backSprite == nullptr)
		return false;
	if (m_bInit == false)
		return false;

	this->removeAllChildren();
	CC_SAFE_DELETE(backSprite);
	m_bInit = false;
	this->removeFromParentAndCleanup(true);
	return true;
}

void BackLayer::BackButtonCallback(Ref* pSender)
{
	Destroy();
}

void BackLayer::yesButtonCallback(Ref* pSender)
{
	Destroy();
	CCDirector::sharedDirector()->resume();
	Director::getInstance()->replaceScene(chgame::createScene());
}