#include"stdafx.h"

void A::Hero::init(Layer * pLayer)
{
	m_pLayer = pLayer;

	m_pStand = new GH::CGHSprite;
	m_pStand->initWithFile("stand_ch.png");
	m_pStand->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	pLayer->addChild(m_pStand);
	m_bPalyingAni = false;
	m_pAni = nullptr;
}
void A::Hero::Update(float dt)
{
	if (m_bPalyingAni == true)
	{
		if (m_pAni->getFrame() == m_nMaxFrame - 1)
		{
			m_bPalyingAni = false;
			m_pAni->setVisible(false);
			m_pAni->Destroy();
			CC_SAFE_DELETE(m_pAni);
			m_pStand->setVisible(true);
		}
		else
		{
			m_pAni->AnimationUpdate(dt);
		}
	}
}
void A::Hero::playAni(E_STATE est, Point pos)
{
	m_bPalyingAni = true;
	m_pStand->setVisible(false);
	if (m_pAni == nullptr)
	{
		m_pAni = new GH::CGHAnimation(m_pLayer);
		switch (est)
		{
		case E_STATE::STAND:
			m_pAni->Init("texture_ch0", "stand_ch_", ".png", 4, 0);
			m_nMaxFrame = 4;
			break;
		case E_STATE::LEFT:
			m_pAni->Init("texture_ch2", "Right_ch_", ".png", 3, 0);
			m_nMaxFrame = 3;
			break;
		case E_STATE::RIGHT:
			m_pAni->Init("texture_ch1", "Left_ch_", ".png", 3, 0);
			m_nMaxFrame = 3;
			break;
		default:
			m_pAni->Init("texture_ch0", "stand_ch_", ".png", 4, 0);
			m_nMaxFrame = 4;
			break;
		}
		m_pAni->setPosition(pos);
		m_pAni->setAniTime(0.1f);
		m_pAni->setLayer(10);
		m_pAni->setLoopCount(1);
		m_pAni->setEffect(true);
		m_pAni->addChild();
	}
	
}