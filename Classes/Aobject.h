#pragma once


class Aobject
{
private:
	int m_nObWid;
	int m_nObHei;
	int m_size;
	GH::CGHSprite * m_pAni;
	Vec2 m_cPos;
	Vec2 m_cPrPos;
	Point m_cMoveBuf;
	Rect m_vRect;
	bool m_direction;
	
public:
	Aobject(){}
	~Aobject(){}

	bool Init(Layer * pLayer, Vec2 ind, int objectNum);
	void Destroy();

	void Move(Vec2 mpos);
	void MoveCancel();

	void setPos(Point pos);
	Sprite * getSprite();

	Vec2 getIndex();
	void setIndex(Vec2 ind);

	int getsize();
	bool getdirection();
};