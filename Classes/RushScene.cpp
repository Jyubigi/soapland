#include "stdafx.h"


/**
@brief : 초기화 하는 구문
@param : 없음
*/

RushScene::RushScene()
{
}
RushScene::~RushScene()
{
}

bool RushScene::init()
{
	//!< 최상위 레이어에서 초기화를 시행하고 초기화가 정상적으로 되었는지 확인
	//!< 만약 false 를 하게 되면 정상적이지 않은걸로 판단하고 이하의 구문 실행 안함
	//오브젝트 할당
	if (!CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)))
		return false;

	//!< 화면 사이즈 알아오는 구문
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//!< 화면 중심 위치 알아오는 구문
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto sprite = new GH::CGHSprite;
	sprite->initWithFile("ingame.png");
	sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	this->addChild(sprite, 0);


	auto back = new GH::CGHSprite;
	back->initWithFile("image.png");
	this->addChild(back);
	back->setPosition(ccp(250, 350));

	CCDirector::sharedDirector()->resume();

	log("log : RushScene init");

	int             nTileNum = 0;
	ssize_t 	    bufferSize;

	m_cMovingIndex.x = -1;
	m_cMovingIndex.y = -1;

	m_nObCount = 0;
	char buf[64];
	srand((unsigned int)time(NULL));
	stage = rand()%7 + 1;
	m_strFile = AC::MapStorage::getInstance()->getMapAt(stage);
	log(m_strFile.c_str());
	//CCLog("^^^^^^^^^ %d ^^^^^^^^^^^^", ran);
	for (auto i : m_pBoxes)
	{
		i = 0;
	}
	int x = 0, y = 0;
	int pointchar = 0;
	while (1)//오브젝트 생성
	{
		log("log : now %c", m_strFile[pointchar]);
		if (m_strFile[pointchar] == '\n')
		{
			y++;
			pointchar++;
			x = 0;
		}
		else if (m_strFile[pointchar] >= '0' && m_strFile[pointchar] <= '9')
		{
			int number = m_strFile[pointchar] - '0';
			if (number < 2)
			{
				pointchar++;
				x++;
				continue;
			}
			int size = number / 2;
			log("log : object init = %d", number);
			Aobject* aobject = new Aobject;
			aobject->Init(this, ccp(x, y), number);
			m_pObjects.push_back(aobject);

			if (number % 2)
			{// 1
				// 세로
				int j = 0;
				for (j = 0; j < size; j++)
					m_pBoxes[y + j][x] = 1;
			}
			else// 0 
			{
				// 가로
				int j = 0;
				for (j = 0; j < size; j++)
					m_pBoxes[y][x + j] = 1;
			}

			pointchar++;
			x++;
		}
		else if (m_strFile[pointchar] == '\0')
		{
			break;
		}
		else
		{
			pointchar++;
		}
	}

	A::ScoreCounter::getInstance()->init(E_GAME_NUM::RUSH);

	this->setKeypadEnabled(true);

	listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchMoved = CC_CALLBACK_2(RushScene::onTouchMoved, this);
	listener->onTouchBegan = CC_CALLBACK_2(RushScene::onTouchBegan, this);
	listener->onTouchCancelled = CC_CALLBACK_2(RushScene::onTouchCancelled, this);
	listener->onTouchEnded = CC_CALLBACK_2(RushScene::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	schedule(schedule_selector(RushScene::Update), 0.0f);
	return true;
}
void RushScene::onExit()
{
	for (auto i : m_pBoxes)
	{
		i = 0;
	}
	for (auto i : m_pObjects)
	{
		i->Destroy();
		CC_SAFE_DELETE(i);
	}
	this->removeAllChildrenWithCleanup(true);
	log("log : RushScene destroy");
}
void RushScene::Update(float dt)
{
	A::ScoreCounter::getInstance()->update(dt);

}

bool RushScene::onTouchBegan(Touch* touch, Event* unused_event)//어느 오브젝트를 터치헸는지 판별
{
	log("log : RushScene touch");
	int i = 0;
	for (i = 0; i < m_pObjects.size(); i++)
	{
		if (m_pObjects.at(i)->getSprite()->getBoundingBox().containsPoint(touch->getLocation()))
		{
			Began(i);
			return true;
		}
	}
	return false;
}

void RushScene::onTouchMoved(Touch* touch, Event* unused_event)//여기서 충돌처리
{
	Vec2 pos = touch->getLocation();
	Vec2 spos = tempsprite->getPosition();
	if (tempdirection)
	{
		// 홀 = 세로
		spos.y = pos.y;
		if (collimin <= spos.y)
			spos.y = collimin;
		if (collimax >= spos.y)
			spos.y = collimax;
	}
	else{
		//짝 = 가로
		spos.x = pos.x;
		if (collimin >= spos.x)
		{
			spos.x = collimin;
			if (tempsize == 1 && collimin <= (float)(BACKPOSX + TILESIZE / 2))
			{
				this->gemeOver();
			}
		}
		if (collimax <= spos.x)
			spos.x = collimax;
	}

	tempsprite->setPosition(spos);
}

void RushScene::onTouchEnded(Touch* touch, Event* unused_event)//오브젝트 데이터를 좌표에따라 옮김
{
	Vec2 spos = tempsprite->getPosition();
	spos = putPointInGrid(spos, tempsize, tempdirection);
	tempsprite->setPosition(spos);
	Vec2 ind = sindexFinding(spos, tempsize, tempdirection);

	m_pObjects.at(tempnum)->setIndex(ind);

	int k;
	if (tempdirection == true)
	{
		for (k = 0; k < tempsize; k++)
			m_pBoxes[(int)ind.y + k][(int)ind.x] = 1;
	}
	else
	{
		for (k = 0; k < tempsize; k++)
			m_pBoxes[(int)ind.y][(int)ind.x + k] = 1;
	}
}

void RushScene::onTouchCancelled(Touch* touch, Event* unused_event)
{
	onTouchEnded(touch , unused_event);
}

Point RushScene::putPointInGrid(Point pos, int size, bool direc)
{
	int buf = 0, tpos = 0;

	if (direc == true)
	{
		buf = pos.x;
		tpos = pos.y - BACKPOSY;
	}
	else
	{
		buf = pos.y;
		tpos = pos.x - BACKPOSX;
	}
	if (size % 2 == 0)
	{
		if ((int)tpos % TILESIZE > TILESIZE / 2)
		{
			tpos -= tpos % TILESIZE;
			tpos += TILESIZE;
		}
		else if (tpos % TILESIZE <= TILESIZE / 2)
		{
			tpos -= tpos % TILESIZE;
		}
	}
	else
	{
		tpos -= tpos % TILESIZE;
		tpos += TILESIZE / 2;
	}

	if (direc == true)
	{
		return ccp((float)buf, (float)tpos + BACKPOSY);
	}
	else
	{
		return ccp((float)tpos + BACKPOSX, (float)buf);
	}
}
Vec2 RushScene::sindexFinding(Point pos, int size, bool direc)
{
	if (size % 2 == 0)
	{
		if (direc == true)
		{
			pos.y += TILESIZE / 2;
		}
		else
		{
			pos.x -= TILESIZE / 2;
		}
	}
	int x = ((int)pos.x - (int)BACKPOSX) / TILESIZE;
	int y = ((int)pos.y - (int)BACKPOSY) / -TILESIZE;//인덱스값 찾기

	if (((int)pos.x - (int)BACKPOSX) % TILESIZE > TILESIZE / 2)
	{
		x++;
	}

	if ((int)(pos.y - BACKPOSY) % -TILESIZE < -TILESIZE / 2)
	{
		y++;
	}

	Vec2 ind;
	ind.x = x;
	ind.y = y;

	if (direc == true)
	{
		ind.y -= (size / 2 - (1 - size % 2));
	}
	else
	{
		ind.x -= (size / 2 - (1 - size % 2));
	}
	ind.y += MAPHEIGHT - 1;
	return ind;
}

void RushScene::Began(int i)
{
	tempdirection = m_pObjects.at(i)->getdirection();
	tempsprite = m_pObjects.at(i)->getSprite();
	tempsize = m_pObjects.at(i)->getsize();
	tempnum = i;

	m_nowcar = i;
	int pos;

	if (tempdirection)
	{
		// 세로
		collimin = BACKPOSY + BACKHIGHT - (TILESIZE / 2 * tempsize);
		collimax = BACKPOSY + (TILESIZE / 2 * tempsize);
		pos = m_pObjects.at(i)->getIndex().y;
		int x = m_pObjects.at(i)->getIndex().x;
		int min = pos,
			max = pos + tempsize - 1;

		int k = min;

		for (k = min; k <= max; k++)
		{
			m_pBoxes[k][x] = 0;
		}

		for (k = max; k < MAPHEIGHT; k++)// 아래쪽 탐색
		{
			if (m_pBoxes[k][x] == 1)
			{
				collimax += (MAPHEIGHT - k) * TILESIZE;
				break;
			}
		}
		for (k = min; k >= 0; k--)// 위쪽 탐색
		{
			if (m_pBoxes[k][x] == 1)
			{
				collimin -= (k + 1) * TILESIZE;
				break;
			}
		}


	}
	else
	{
		collimin = BACKPOSX + (TILESIZE / 2 * tempsize);
		collimax = BACKPOSX + BACKWIDTH - (TILESIZE / 2 * tempsize);
		int y = m_pObjects.at(i)->getIndex().y;
		pos = m_pObjects.at(i)->getIndex().x;// 가로
		int k;
		int min = pos, max = pos + tempsize - 1;

		for (k = min; k <= max; k++)//맵 지우기
		{
			m_pBoxes[y][k] = 0;
		}

		for (k = min; k >= 0; k--)//왼쪽탐색
		{
			if (m_pBoxes[y][k] == 1)
			{
				collimin += (k + 1) * TILESIZE;
				break;
			}
			/// left
		}
		for (k = max; k < MAPWIDTH; k++)//오른쪽 탐색
		{
			if (m_pBoxes[y][k] == 1)
			{
				collimax -= (MAPWIDTH - k) * TILESIZE;
				break;
			}
			//right
		}

	}// 여기까지 맵에서 지우기소스 

}
