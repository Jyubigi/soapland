#include"stdafx.h"

void Enemy::create(Point pos)
{
	m_bActive = true;
	stone->setVisible(true);
	m_cPos = pos;
}

void Enemy::init(Layer *   pLayer)
{
	m_bActive = false;
	stone = new GH::CGHSprite;
	stone->initWithFile("jelly/jelly.png");
	m_cPos = (ccp(-300, 200));
	stone->setPosition(m_cPos);
	pLayer->addChild(stone);
}

void Enemy::update(float dt)
{
	int num;
	int speed;
	srand(unsigned(time(NULL)));
	num = rand() % 3;
	if (num == 0)
	{
		speed = 500;
	}
	else if (num == 1)
	{
		speed = 500;
	}
	else if (num == 2)
	{
		speed = 500;
	}
	else if (num == 3)
	{
		speed = 500;
	}
	m_cPos.x -= dt * speed;
	stone->setPosition(m_cPos);

	if (m_cPos.x < 0)
	{
		score++;
	}
	rect_2 = Rect(m_cPos.x, m_cPos.y-20, 80, 83);
}
void Enemy::Die()
{
	stone->setVisible(false);
	m_bActive = false;
}
Point Enemy::getPos()
{
	return m_cPos;
}
Rect Enemy::getRect()
{
	return rect_2;
}