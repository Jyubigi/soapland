#pragma once

enum class E_STATE
{
	STAND =0, LEFT, RIGHT
};

namespace A
{
	class Hero
	{
	private:
		Layer * m_pLayer;

		GH::CGHSprite * m_pStand;
		GH::CGHAnimation * m_pAni;

		bool m_bPalyingAni;
		int m_nMaxFrame;

	public:
		void init(Layer * pLayer);
		void Update(float dt);

		void playAni(E_STATE est, Point pos);
	};
}