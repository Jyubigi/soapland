#pragma once

#define D_DESIGN_WIDTH 480
#define D_DESIGN_HEIGHT 800

#define D_WINDOWS_SHOW 1

#define D_GAME_NAME "Game"

#define MAPWIDTH 5
#define MAPHEIGHT 5

#define D_ENEMY_MAX 100

#define TILESIZE 80
#define BACKPOSX 50
#define BACKPOSY 150
#define BACKHIGHT 400
#define BACKWIDTH 400

#define YOKANWID 100
#define YOKANHEI 100

#define RUSH_MAXTIME 60;

#define CREATE_SCENE_FUNC(__TYPE__) \
	static Scene* createScene() \
{ \
	log("log : create"); \
	auto scene = Scene::create(); \
	auto layer = __TYPE__::create(); \
	scene->addChild(layer); \
	return scene; \
}
enum class E_GAME_NUM : unsigned int
{
	RUSH,AROMA,JELLY,YOKAN
};