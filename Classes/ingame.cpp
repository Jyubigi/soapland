#include"stdafx.h"

void ingame::init(Layer * pLayer)
{
	state = false;
	m_bJumpOn = false;
	m_pAni = new GH::CGHAnimation(pLayer);
	m_pAni->Init("jelly/texture_run", "r", ".png", 5, 1);
	m_pAni->setPosition(ccp(54, 340));
	m_pAni->setAniTime(0.1f);
	m_pAni->setLayer(10);
	m_pAni->addChild();
	m_cPos=ccp(100, 200);
	
}
void ingame::update(float dt)
{
	m_pAni->setPosition(m_cPos);
	m_pAni->AnimationUpdate(dt);

	if (num == 0)
	{
		if (state == false)
		{
			state = true;
			m_bJumpOn = true;
		}
		else if (state == true)
		{
			if (m_cPos.y > 400)
			{
				m_bJumpOn = false;
			}
			if (m_bJumpOn == true)
			{
				m_cPos.y += 9.8*dt * 100;
			}
			else if (m_bJumpOn == false)
			{
				if (m_cPos.y < 200)
				{
					m_cPos.y = 200;
					state = false;
					num = 1;
				}
				else
				{
					m_cPos.y -= 9.8*dt * 50;
				}
			}
		}
	}
	rect_1 = Rect(m_cPos.x-15, m_cPos.y, 60, 90);
}
void ingame::pos(int pos1)
{
	num = pos1;
}
Point ingame::getPos()
{
	return m_cPos;
}
Rect ingame::getRect()
{
	return rect_1;
}
