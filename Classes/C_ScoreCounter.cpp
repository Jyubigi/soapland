#include "stdafx.h"

void A::ScoreCounter::init(E_GAME_NUM gamenum)
{
	m_eGameNum = gamenum;
	m_nScore = 0;
	m_fTime = 0.0f;

	switch (m_eGameNum)
	{
	case E_GAME_NUM::AROMA:
		m_strInkey = "AromaHigh";
		break;
	case E_GAME_NUM::JELLY:
		m_strInkey = "JellyHigh";
		break;
	case E_GAME_NUM::RUSH:
		m_strInkey = "RushHigh";
		break;
	case E_GAME_NUM::YOKAN:
		m_strInkey = "YokanHigh";
		break;
	default:
		break;
	}

	m_nBest = UserDefault::getInstance()->getIntegerForKey((char*)m_strInkey.c_str());
}
E_GAME_NUM A::ScoreCounter::getGameNum()
{
	return m_eGameNum;
}

void A::ScoreCounter::update(float dt)
{
	m_fTime += dt;
}

void A::ScoreCounter::timeIsScore()
{
	m_nScore = (int)m_fTime;
}

void A::ScoreCounter::takeScore(int sco)
{
	m_nScore += sco;
}
int A::ScoreCounter::getBestScore() const
{
	return UserDefault::getInstance()->getIntegerForKey((char*)m_strInkey.c_str());
}
int A::ScoreCounter::getScore()
{
	switch (m_eGameNum)
	{
	case E_GAME_NUM::RUSH:
		timeIsScore();
		if (m_nBest == 0 || m_nScore <= m_nBest)
		{
			UserDefault::getInstance()->setIntegerForKey((char*)m_strInkey.c_str(), m_nScore);
		}
		break;
	case E_GAME_NUM::JELLY:
		timeIsScore();
	case E_GAME_NUM::YOKAN:
	case E_GAME_NUM::AROMA:
		if (m_nScore >= m_nBest)
		{
			UserDefault::getInstance()->setIntegerForKey((char*)m_strInkey.c_str(), m_nScore);
		}
		break;
	default:
		break;
	}
	
	return m_nScore;
}
float A::ScoreCounter::getTime() const
{
	return m_fTime;
}