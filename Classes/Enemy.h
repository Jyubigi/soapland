#pragma once

#include"stdafx.h"

class Enemy
{
private:

	GH::CGHSprite * stone;
	GH::CGHSprite * line;
	Point m_cPos;
	int score;
	Rect rect_2;
public:
	bool		m_bActive;
	void init(Layer *	pLayer);
	void update(float dt);
	void pos(Point pos1);
	void create(Point pos);
	void Die();
	Point getPos();
	Rect getRect();
	Enemy(){}
	~Enemy(){}

};