#pragma once

#include "cocos2d.h"
#include <vector>
#include <map>
#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <random>

USING_NS_CC;
using namespace std;

#include "CGHSprite.h"
#include "cGHAnimation.h"

#include "T_Singleton.h"

#include "AC_MapStorage.h"
#include "C_ScoreCounter.h"
#include "C_Swyper.h"

#include "C_Object.h"
#include "Aobject.h"

#include "AC_BackLayer.h"
#include "exitLayer.h"

#include "C_Hero.h"
#include "C_Yokan.h"
#include "C_YokanManager.h"

#include "hero.h"
#include "Monster.h"

#include "ingame.h"
#include "Enemy.h"

#include "AppDelegate.h"

#include "ResultScene.h"
#include "AT_GameScene.h"
#include "AT_Loading.h"

#include "optionLayer.h"
#include "storyLayer.h"
#include "creditLayer.h"

#include "chgame.h"
#include "HelloWorldScene.h"

#include "title.h"

#include "JellyTip.h"
#include "aromaTip.h"
#include "yokanTip.h"
#include "RushTip.h"

#include "RushScene.h"
#include "YokanScene.h"
#include "MiniScene.h"
#include "Mini2Scene.h"
#include "C_Popup.h"
#include "logo.h"
