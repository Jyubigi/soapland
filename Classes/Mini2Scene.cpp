#include "stdafx.h"

/**
@brief : 초기화 하는 구문
@param : 없음
*/
bool Mini2Scene::init()
{
	//!< 최상위 레이어에서 초기화를 시행하고 초기화가 정상적으로 되었는지 확인
	//!< 만약 false 를 하게 되면 정상적이지 않은걸로 판단하고 이하의 구문 실행 안함
	if (!CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)))
		return false;

	CCDirector::sharedDirector()->resume();

	auto left = MenuItemImage::create("left.png", "left1.png",
		CC_CALLBACK_1(Mini2Scene::left, this));
	auto right = MenuItemImage::create("right.png", "right1.png",
		CC_CALLBACK_1(Mini2Scene::right, this));
	left->setPosition(120, 74.5);
	right->setPosition(360, 74.5);

	auto back = new GH::CGHSprite;
	back->initWithFile("aroma/bg.png");
	back->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(back, 0);

	auto leftbutton = Menu::create(left, NULL);
	auto rightbutton = Menu::create(right, NULL);
	leftbutton->setPosition(Vec2::ZERO);
	rightbutton->setPosition(Vec2::ZERO);

	this->addChild(leftbutton, 2);
	this->addChild(rightbutton, 2);
	createcount = 0;
	gametime = 0.0f;

	m_hero = new hero();
	m_hero->init(this);

	for (int i = 0; i < D_ENEMY_MAX; i++)
	{
		m_Enemy[i] = new Monster();
		m_Enemy[i]->init(this);
	}
	//!< 화면 사이즈 알아오는 구문
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//!< 화면 중심 위치 알아오는 구문
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	A::ScoreCounter::getInstance()->init(E_GAME_NUM::AROMA);

	schedule(schedule_selector(Mini2Scene::Update), 0.0f);

	this->setKeypadEnabled(true);

	listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(Mini2Scene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(Mini2Scene::onTouchMoved, this);
	listener->onTouchCancelled = CC_CALLBACK_2(Mini2Scene::onTouchCancelled, this);
	listener->onTouchEnded = CC_CALLBACK_2(Mini2Scene::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void Mini2Scene::Update(float dt)
{
	A::ScoreCounter::getInstance()->update(dt);
	gametime += dt;
	m_hero->update(dt);
	for (int i = 0; i < D_ENEMY_MAX; i++)
	{
		m_Enemy[i]->update(dt);
		if (m_Enemy[i]->m_bActive==true && m_Enemy[i]->getPos().y <= 120.0f)//점수계산ㅇㅇ
		{
			m_Enemy[i]->m_bActive = false;
			A::ScoreCounter::getInstance()->takeScore(2);
		}
		if (RectVsRectCollision(m_hero->getPos(), m_Enemy[i]->getPos(),
			m_hero->getRect(), m_Enemy[i]->getRect()) && m_Enemy[i]->m_bActive == true || //
			RectVsRectCollision(m_hero->getPos(), m_Enemy[i]->getPos2(),
			m_hero->getRect(), m_Enemy[i]->getRect2()) && m_Enemy[i]->m_bActive == true)
		{
			this->gemeOver();
		}
		if (gametime >= 3.0f && createcount < D_ENEMY_MAX)
		{
			m_Enemy[createcount]->create(ccp(80, 1000), ccp(400, 1000));
			createcount++;
			gametime = 0.0f;
		}
		if (createcount > 99)
		{
			createcount = 0;
		}
	}
}

bool Mini2Scene::onTouchBegan(Touch* touch, Event* unused_event)
{
	CCLog("touch");
	return true;

}
void Mini2Scene::onTouchMoved(Touch* touch, Event* unused_event)
{
	Point location = touch->getLocation();
	//m_ingame->pos(location);
	//int a, b;
	//a=location.x;
	//b = location.y;d
	//CCLog("%d %d", a,b);
}
void Mini2Scene::onTouchCancelled(Touch* touch, Event* unused_event)
{

}
void Mini2Scene::onTouchEnded(Touch* touch, Event *unused_event)
{

}
void Mini2Scene::left(Ref* pSender)
{
	m_hero->pos(0);
}
void Mini2Scene::right(Ref* pSender)
{
	m_hero->pos(1);
}
bool Mini2Scene::RectVsRectCollision(Point target1, Point target2, Rect rect1, Rect rect2)
{
	if (rect1.origin.x <= rect2.origin.x + rect2.size.width &&
		rect1.origin.x + rect1.size.width >= rect2.origin.x &&
		rect1.origin.y - rect1.size.height <= rect2.origin.y &&
		rect1.origin.y >= rect2.origin.y - rect2.size.height)
		return true;
	return false;

}