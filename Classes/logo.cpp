#include "stdafx.h"

Scene* logo::createScene()
{
	//!< 씬을 Auto Release 가능하도록 Create 메소드로 만듬
	//!< 씬을 Retain 메소드와 Auto Release 메소드에 등록
	auto scene = Scene::create();

	//!< 씬에 Layer를 생성해서 등록시킴
	auto layer = logo::create();

	//!< 현재 씬 리스트에 등록
	scene->addChild(layer);
	
	//!< 이 씬에 대한 정보를 반환
	return scene;
}

bool logo::init()
{
	if (!Layer::init())
		return false;

	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}
	m_bNext = false;

	m_pAni = new GH::CGHAnimation(this);
	bool bChk = m_pAni->Init("UI/texture_logo", "logo_", ".png", 23, 0);
	if (!bChk) CCLog("Error!");
	m_pAni->setPosition(ccp(D_DESIGN_WIDTH/2,D_DESIGN_HEIGHT/2));
	m_pAni->setAniTime(0.1f);
	m_pAni->setLayer(10);
	m_pAni->setLoopCount(1);
	m_pAni->setEffect(true);
	m_pAni->addChild();

	log("log : logo init...");
	schedule(schedule_selector(logo::Update), 0.1f);

	
	return true;
}

void logo::Update(float dt)
{
	if (!m_bNext)
	{
		m_pAni->AnimationUpdate(dt);
		if (m_pAni->getFrame() == 22)
		{
			next();
		}
	}
}

void logo::next()
{
	log("log : logo next...");
	m_pAni->Destroy();
	CC_SAFE_DELETE(m_pAni);
	//Scene *pScene = TransitionCrossFade::create(1.0f, title::createScene());
	//Director::getInstance()->replaceScene(pScene);
	//Layer* pLayer = new title();
	//addChild(pLayer);
	Scene * sn = title::createScene();
	Director::getInstance()->replaceScene(sn);
	//pLayer->release();

	m_bNext = true;
}