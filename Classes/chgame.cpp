#include "stdafx.h"

Scene* chgame::createScene()
{
	//!< 씬을 Auto Release 가능하도록 Create 메소드로 만듬
	//!< 씬을 Retain 메소드와 Auto Release 메소드에 등록
	auto scene = Scene::create();

	//!< 씬에 Layer를 생성해서 등록시킴
	auto layer = chgame::create();

	//!< 현재 씬 리스트에 등록
	scene->addChild(layer);

	//!< 이 씬에 대한 정보를 반환
	return scene;
}
bool chgame::init()
{
	if (!Layer::init())
		return false;
	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}
	/*spr = new GH::CGHSprite;
	spr->initWithFile("UI/option.png");
	spr->setPosition(m_cPos);
	this->addChild(spr, 0);*/
	if (!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("main.mp3", true);
	}

	auto m1 = MenuItemImage::create("UI/yokangame.png", "UI/yokangame.png", CC_CALLBACK_1(chgame::game1, this));
	auto m2 = MenuItemImage::create("UI/rushgame.png", "UI/rushgame.png", CC_CALLBACK_1(chgame::game2, this));
	auto m3 = MenuItemImage::create("UI/jellygame.png", "UI/jellygame.png", CC_CALLBACK_1(chgame::game3, this));
	auto m4 = MenuItemImage::create("UI/aromagame.png", "UI/aromagame.png", CC_CALLBACK_1(chgame::game4, this));
	m1->setZOrder(1);
	m2->setZOrder(1);
	m3->setZOrder(1);
	m4->setZOrder(1);
	m1->setPosition(ccp(143, 556));
	m2->setPosition(ccp(143, 366));
	m3->setPosition(ccp(330, 556));
	m4->setPosition(ccp(330, 366));
	auto mm1 = Menu::create(m1, NULL);
	auto mm2 = Menu::create(m2, NULL);
	auto mm3 = Menu::create(m3, NULL);
	auto mm4 = Menu::create(m4, NULL);
	mm1->setPosition(Vec2::ZERO);
	mm2->setPosition(Vec2::ZERO);
	mm3->setPosition(Vec2::ZERO);
	mm4->setPosition(Vec2::ZERO);
	this->addChild(mm1, 2);
	this->addChild(mm2, 2);
	this->addChild(mm3, 2);
	this->addChild(mm4, 2);

	auto option = MenuItemImage::create("UI/option.png", "UI/option1.png", CC_CALLBACK_1(chgame::optionCallBack, this));
	auto creater = MenuItemImage::create("UI/creater.png", "UI/creater1.png", CC_CALLBACK_1(chgame::createrCallBack, this));
	auto story = MenuItemImage::create("UI/story.png", "UI/story1.png", CC_CALLBACK_1(chgame::storyCallBack, this));
	option->setPosition(ccp(230, 138));
	creater->setPosition(ccp(230, 68));
	story->setPosition(ccp(230, 208));
	auto om = Menu::create(option, NULL);
	auto cm = Menu::create(creater, NULL);
	auto sm = Menu::create(story, NULL);
	om->setPosition(Vec2::ZERO);
	cm->setPosition(Vec2::ZERO);
	sm->setPosition(Vec2::ZERO);
	this->addChild(om, 3);
	this->addChild(cm, 3);
	this->addChild(sm, 3);
	schedule(schedule_selector(chgame::Update), 0.0f);

	this->setKeypadEnabled(true);
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(chgame::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	return true;
}

void chgame::Update(float dt)
{
	/*spr->setPosition(m_cPos);*/
}

bool chgame::onTouchBegan(Touch* touch, Event* unused_event)
{
	Point location = touch->getLocation();
	m_cPos = location;
	int x, y;
	x = m_cPos.x;
	y = m_cPos.y;
	log("%d %d", x, y);
	return true;
}
void chgame::onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event)
{
	if (!m_pExit)
	{
		m_pExit = new AC::exitLayer;
		m_pExit->Init(this);
	}
	else if (m_pExit)
	{
		m_pExit->Destroy();
		CC_SAFE_DELETE(m_pExit);
	}
}
void chgame::optionCallBack(Ref* pSender)
{
	auto pl = new optionLayer;
	pl->Init(this);
	this->addChild(pl,20);
}
void chgame::createrCallBack(Ref* pSender)
{
	auto pl = new creditLayer;
	pl->init(this);
	this->addChild(pl, 20);
}
void chgame::storyCallBack(Ref* pSender)
{
	auto pl = new storyLayer;
	pl->init(this);
	this->addChild(pl, 20);
}
void chgame::game1(Ref* pSender)
{
	CCLog("ㅇㅇ");
	Director::getInstance()->replaceScene(YokanTip::createScene());
}
void chgame::game2(Ref* pSender)
{
	CCLog("ㅇㅇ1");

	Director::getInstance()->replaceScene(RushTip::createScene());
}
void chgame::game3(Ref* pSender)
{
	CCLog("ㅇㅇ2");
	Director::getInstance()->replaceScene(JellyTip::createScene());
}
void chgame::game4(Ref* pSender)
{
	CCLog("ㅇㅇ3");
	Director::getInstance()->replaceScene(AromaTip::createScene());
}