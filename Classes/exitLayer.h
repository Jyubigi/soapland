#pragma once
namespace AC
{
	class exitLayer : public cocos2d::Layer
	{
	private:
		GH::CGHSprite * backSprite;
		Layer * m_pscene;
	public:
		exitLayer()
		{
			backSprite = nullptr;
		}
		~exitLayer(){}
	public:
		bool Init(Layer * pLayer);
		bool Destroy();

		bool isDestroyed();

		virtual bool onTouchBegan(Touch* touch, Event* unused_event);
		void closeCallback(Ref* pSender);
		void noCallback(Ref * pSander);
		virtual bool init(){ return true; }
		CREATE_FUNC(exitLayer);
	};
}