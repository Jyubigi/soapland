#pragma once

enum class E_YOKAN : unsigned int
{
	NOMAL = 0, SWEET=1,GREEN=2
};


namespace A
{
	class Yokan : public A::Object
	{
	private:
		E_YOKAN m_eType;
		int m_nPoint;
		float m_fSpeed;

	public:
		Yokan(){}
		~Yokan(){}

		virtual void init(Layer * pLayer, E_YOKAN type = E_YOKAN::NOMAL);
		void Update(float dt);

		void setType(E_YOKAN yotype);

		float getSpeed() const;
		void setSpeed(float speed);

		int getPoint() const;
	};
}