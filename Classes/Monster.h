#pragma once

#include"stdafx.h"

class Monster
{
private:
	GH::CGHSprite * stone;
	GH::CGHSprite * stone2;
	Point m_cPos;
	Point m_cPos2;
	int speed;
	Rect rect_1;
	Rect rect_2;
public:
	bool		m_bActive;
	void init(Layer *	pLayer);
	void update(float dt);
	void pos(Point pos1);
	void create(Point pos, Point pos2);
	void Die();
	Point getPos();
	Point getPos2();
	Rect getRect();
	Rect getRect2();
	Monster();
	~Monster();

};