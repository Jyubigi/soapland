#include "stdafx.h"

void A::Swyper::onTouchBegun(Point tpos)
{
	m_cInitPos = tpos;
}
void A::Swyper::onTouchEnded(Point tpos)
{
	m_cCurPos = tpos;
}
E_DIRECTION A::Swyper::getDirection()
{
	float xp = m_cInitPos.x - m_cCurPos.x;
	float yp = m_cInitPos.y - m_cCurPos.y;


	if (fabs(xp) > fabs(yp))
	{
		if (xp > 0.0f)
		{
			return E_DIRECTION::RIGHT;
		}
		else
		{
			return E_DIRECTION::LEFT;
		}

	}
	else if (fabs(xp) < fabs(yp))
	{
		if (yp > 0.0f)
		{
			return E_DIRECTION::DOWN;
		}
		else
		{
			return E_DIRECTION::UP;
		}
	}

	return E_DIRECTION::NONE;
}