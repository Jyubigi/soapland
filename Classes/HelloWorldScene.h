#pragma once

#include "stdafx.h"

class HelloWorld : public LayerColor
{
private:
	GH::CGHSprite * gm;
	float  count;
	bool m_bDestroyed;

public:
	CREATE_SCENE_FUNC(HelloWorld);
    //!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
    virtual bool init(); 

	void destroy();
    
    //!< 콜백 함수 (이 씬에선 종료할 때 쓰임)
    void menuCloseCallback(Ref* pSender);

	void Update(float dt);

    //!< 씬을 등록 시킴
    CREATE_FUNC(HelloWorld);
};