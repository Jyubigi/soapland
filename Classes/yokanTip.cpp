#include "stdafx.h"

bool YokanTip::init()
{
	if (!Layer::init())
	{
		return false;
	}

	touchcount = 0;
	back[0] = new GH::CGHSprite;
	back[0]->initWithFile("yokan/yokantip.png");
	back[0]->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(back[0], 2);

	back[1] = new GH::CGHSprite;
	back[1]->initWithFile("yokan/yokanstory.png");
	back[1]->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(back[1], 2);

	back[touchcount]->setVisible(false);

	return true;
}
bool YokanTip::onTouchBegan(Touch* touch, Event* unused_event)
{
	touchcount++;
	back[touchcount % 2]->setVisible(false);
	back[1 - touchcount % 2]->setVisible(true);
	if (touchcount > 1)
	{
		Director::getInstance()->replaceScene(YokanScene::createScene());
	}
	return true;
}