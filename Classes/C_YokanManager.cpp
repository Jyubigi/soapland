#include "stdafx.h"

void A::YokanManager::restoreYokans()
{
	m_pYokans[0]->getSprite()->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT + YOKANHEI));//아래로
	m_pYokans[1]->getSprite()->setPosition(ccp(D_DESIGN_WIDTH / 2, -YOKANHEI));//위로
	m_pYokans[2]->getSprite()->setPosition(ccp(-YOKANWID, D_DESIGN_HEIGHT / 2));//->
	m_pYokans[3]->getSprite()->setPosition(ccp(D_DESIGN_WIDTH + YOKANWID, D_DESIGN_HEIGHT / 2));//<-
}
void A::YokanManager::throwYokans(float ran)//이부분 식만 수정하면 된다
{
	GH::CGHSprite * spr = m_pYokans[m_nYokanCur]->getSprite();
	Point pos = spr->getPosition();
	spr->setRotation(spr->getRotation() - ran);
	switch (m_nYokanCur)
	{
	case 0:
		spr->setPosition(pos.x, pos.y - ran);
		break;
	case 1:
		spr->setPosition(pos.x, pos.y + ran);
		break;
	case 2:
		spr->setPosition(pos.x + ran, pos.y);
		break;
	case 3:
		spr->setPosition(pos.x - ran, pos.y);
		break;
	}
}
void A::YokanManager::init(Layer*pLayer)
{
	m_pLayer = pLayer;
	m_nYokanCur = 0;

	for (int i = 0; i < 4; i++)
	{
		m_pYokans[i] = new A::Yokan;
		m_pYokans[i]->init(m_pLayer);
	}
	restoreYokans();
	srand(time(NULL));
}
void A::YokanManager::Update(float dt)
{
	if (m_bYokanIsEaten == true)//양갱이 히어로에게 먹혔을 때
	{
		int randbuf = rand() % 4;
		if (randbuf == m_nYokanCur)
		{
			if (m_nYokanCur == 3)
				randbuf = m_nYokanCur - 1;
			else
				randbuf = m_nYokanCur + 1;
		}
		m_nYokanCur = randbuf;
		m_bYokanIsEaten = false;
		m_pYokans[m_nYokanCur]->setSpeed(m_pYokans[m_nYokanCur]->getSpeed() * 1.15f);
		restoreYokans();
	}
	throwYokans(dt *m_pYokans[m_nYokanCur]->getSpeed());

	for (auto i : m_pYokans)
	{
		i->Update(dt);
	}
}
void A::YokanManager::Destroy()
{
	for (auto i : m_pYokans)
	{
		if (i != nullptr)
		{
			i->Destroy();
			CC_SAFE_DELETE(i);
		}
	}
}

void A::YokanManager::setYokanIsEaten(bool YokanIsEaten)
{
	m_bYokanIsEaten = YokanIsEaten;
}

int A::YokanManager::getNowYokanCur() const
{
	return m_nYokanCur;
}
int A::YokanManager::getNowYokanCoin() const
{
	return m_pYokans[m_nYokanCur]->getPoint();
}
Rect A::YokanManager::getNowYokanRect() const
{
	return Rect(m_pYokans[m_nYokanCur]->getSprite()->getPosition().x, m_pYokans[m_nYokanCur]->getSprite()->getPosition().y, YOKANWID,YOKANHEI);
}