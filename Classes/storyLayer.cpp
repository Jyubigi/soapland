#include "stdafx.h"

void storyLayer::init(Layer * pLayer)
{
	m_pLayer = pLayer;
	auto image = new GH::CGHSprite;
	image->initWithFile("story.png");
	image->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(image);

	auto _ex = MenuItemImage::create("UI/Back.png", "UI/Back1.png", CC_CALLBACK_1(storyLayer::exit, this));
	_ex->setPosition(ccp(D_DESIGN_WIDTH / 2 + 210, D_DESIGN_HEIGHT / 2 + 180));
	auto exm = Menu::create(_ex, NULL);
	exm->setPosition(Vec2::ZERO);
	this->addChild(exm, 5);
	_eventDispatcher->pauseEventListenersForTarget(m_pLayer);

	auto _listener = EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);

	_listener->onTouchBegan = CC_CALLBACK_2(storyLayer::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
}
bool storyLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
	return true;
}
void storyLayer::exit(Ref * pSander)
{
	_eventDispatcher->resumeEventListenersForTarget(m_pLayer);
	removeAllChildren();
	removeFromParentAndCleanup(true);
}