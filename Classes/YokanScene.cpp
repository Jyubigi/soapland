#include "stdafx.h"
using namespace A;
bool YokanScene::init()
{
	if (!CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)))
		return false;

	CCDirector::sharedDirector()->resume();

	m_pHero = new A::Hero;
	m_pHero->init(this);
	YokanManager::getInstance()->init(this);
	A::ScoreCounter::getInstance()->init(E_GAME_NUM::YOKAN);

	this->setKeypadEnabled(true);

	listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchMoved = CC_CALLBACK_2(YokanScene::onTouchMoved, this);
	listener->onTouchBegan = CC_CALLBACK_2(YokanScene::onTouchBegan, this);
	listener->onTouchCancelled = CC_CALLBACK_2(YokanScene::onTouchCancelled, this);
	listener->onTouchEnded = CC_CALLBACK_2(YokanScene::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	schedule(schedule_selector(YokanScene::Update), 0.0f);

	return true;
}

void YokanScene::Update(float dt)
{
	A::ScoreCounter::getInstance()->update(dt);
	YokanManager::getInstance()->Update(dt);
	m_pHero->Update(dt);
	
	if (YokanManager::getInstance()->getNowYokanRect().intersectsRect(Rect(D_DESIGN_WIDTH/2, D_DESIGN_HEIGHT/2, 100, 160)))
	{
		this->gemeOver();
	}
}

bool YokanScene::onTouchBegan(Touch* touch, Event* unused_event)
{
	Swyper::getInstance()->onTouchBegun(touch->getLocation());
	return true;
}
void YokanScene::onTouchMoved(Touch* touch, Event* unused_event)
{
}
void YokanScene::onTouchCancelled(Touch* touch, Event* unused_event)
{
	onTouchEnded(touch,unused_event);
}
void YokanScene::onTouchEnded(Touch* touch, Event *unused_event)
{
	Swyper::getInstance()->onTouchEnded(touch->getLocation());
	E_DIRECTION dbuf = Swyper::getInstance()->getDirection();
	if (((int)dbuf - 1) == YokanManager::getInstance()->getNowYokanCur())
	{
		switch (dbuf)
		{
		case E_DIRECTION::RIGHT:
			m_pHero->playAni(E_STATE::RIGHT, YokanManager::getInstance()->getNowYokanRect().origin);
			break;
		case E_DIRECTION::LEFT:
			m_pHero->playAni(E_STATE::LEFT, YokanManager::getInstance()->getNowYokanRect().origin);
			break;
		default:
			m_pHero->playAni(E_STATE::STAND, YokanManager::getInstance()->getNowYokanRect().origin);
			break;

		}
		YokanManager::getInstance()->setYokanIsEaten(true);
		A::ScoreCounter::getInstance()->takeScore(1);
	}
}