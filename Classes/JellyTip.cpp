#include "stdafx.h"

bool JellyTip::init()
{
	if (!Layer::init())
	{
		return false;
	}

	back[0] = new GH::CGHSprite;
	back[0]->initWithFile("jelly/jellytip.png");
	back[0]->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(back[0], 2);

	back[1] = new GH::CGHSprite;
	back[1]->initWithFile("jelly/jellystory.png");
	back[1]->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(back[1], 2);
	touchcount = 0;
	back[touchcount%2]->setVisible(false);


	return true;
}

bool JellyTip::onTouchBegan(Touch* touch, Event* unused_event)
{
	back[1-touchcount%2]->setVisible(false);
	back[touchcount % 2]->setVisible(true);
	touchcount++;
	if (touchcount > 1)
	{
		Director::getInstance()->replaceScene(MiniScene::createScene());
	}
	return true;
}