#pragma once

namespace A
{
	class YokanManager : public TempleteSingleton<YokanManager>
	{
	private:
		Layer * m_pLayer;

		A::Yokan * m_pYokans[4];
		int m_nYokanCur;

		bool m_bYokanIsEaten;

	private:
		void restoreYokans();
		void throwYokans(float ran);

	public:
		YokanManager(){}
		~YokanManager(){}

		void setYokanIsEaten(bool YokanIsEaten);

		int getNowYokanCur() const;
		int getNowYokanCoin() const;
		Rect getNowYokanRect() const;

		void init(Layer*pLayer);
		void Update(float dt);
		void Destroy();
	};
}