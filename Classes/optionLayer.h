#pragma once

class optionLayer : public Layer
{
private:
	GH::CGHSprite * _speaker;
	GH::CGHSprite * _dispeaker;
	LabelTTF * m_pLabel;
	Layer * m_pl;
	int m_nVol;
public:
	optionLayer(){}
	~optionLayer(){}

	void applyVol();

	void Init(Layer * pl);
	void minusCallBack(Ref * pSander);
	void plusCallBack(Ref * pSander);
	void exitCallBack(Ref * pSander);
	void Destroy();

	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
};