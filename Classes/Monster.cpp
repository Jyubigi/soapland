#include"stdafx.h"

Monster::Monster()
{

}
Monster::~Monster()
{

}

void Monster::create(Point pos, Point pos2)
{
	m_bActive = true;
	stone->setVisible(true);
	m_cPos = pos;
	m_cPos2 = pos2;
}

void Monster::init(Layer *   pLayer)
{
	speed = 250;
	m_bActive = false;
	stone = new GH::CGHSprite;
	stone2 = new GH::CGHSprite;
	stone->initWithFile("Enemy.png");
	stone2->initWithFile("Enemy.png");
	m_cPos = ccp(-100, -100);
	m_cPos2 = ccp(-100, -100);
	stone->setPosition(m_cPos);
	stone2->setPosition(m_cPos2);
	pLayer->addChild(stone);
	pLayer->addChild(stone2);
}

void Monster::update(float dt)
{
	m_cPos.y -= dt * speed;
	m_cPos2.y -= dt * speed;
	stone->setPosition(m_cPos);
	stone2->setPosition(m_cPos2);
	rect_1 = Rect(m_cPos.x, m_cPos.y-20, 90, 130);
	rect_2 = Rect(m_cPos2.x, m_cPos2.y-20, 90, 130);
}
void Monster::Die()
{
	stone->setVisible(false);
	m_bActive = false;
}
Point Monster::getPos()
{
	return m_cPos;
}
Rect Monster::getRect()
{
	return rect_1;
}
Point Monster::getPos2()
{
	return m_cPos2;
}
Rect Monster::getRect2()
{
	return rect_2;
}