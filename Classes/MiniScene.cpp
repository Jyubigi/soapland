#include "stdafx.h"

/**
@brief : 초기화 하는 구문
@param : 없음
*/

MiniScene::MiniScene()
{
}
bool MiniScene::init()
{
	//!< 최상위 레이어에서 초기화를 시행하고 초기화가 정상적으로 되었는지 확인
	//!< 만약 false 를 하게 되면 정상적이지 않은걸로 판단하고 이하의 구문 실행 안함
	if (!CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)))
		return false;

	CCDirector::sharedDirector()->resume();

	//!< 화면 사이즈 알아오는 구문
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//!< 화면 중심 위치 알아오는 구문
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//!< 폰트 생성
	//!< 첫번째 인자 : 띄울 이름
	//!< 두번째 인자 : 글꼴
	//!< 세번째 인자 : 크기

	coinScore[100] = { 0 };
	score = 0;
	touchcount = 0;
	log("%d", highscore);
	//!< 위치 설정
	
	createcount = 0;
	gametime = 0.0f;

	m_ingame = new ingame();
	m_ingame->init(this);

	for (int i = 0; i < D_ENEMY_MAX; i++)
	{
		m_Enemy[i] = new Enemy();
		m_Enemy[i]->init(this);
	}

	A::ScoreCounter::getInstance()->init(E_GAME_NUM::JELLY);

	this->setKeypadEnabled(true);

	listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(MiniScene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(MiniScene::onTouchMoved, this);
	listener->onTouchCancelled = CC_CALLBACK_2(MiniScene::onTouchCancelled, this);
	listener->onTouchEnded = CC_CALLBACK_2(MiniScene::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	schedule(schedule_selector(MiniScene::Update), 0.0f);

	return true;
}

void MiniScene::Update(float dt)
{
	A::ScoreCounter::getInstance()->update(dt);
	gametime += dt;
	m_ingame->update(dt);
	for (int i = 0; i < D_ENEMY_MAX; i++)
	{
		m_Enemy[i]->update(dt);
		if (RectVsRectCollision(m_ingame->getRect(), m_Enemy[i]->getRect()) && m_Enemy[i]->m_bActive == true)
		{
			CCLog("asdasdasdasdasd");
			this->gemeOver();
		}
		if (m_Enemy[i]->getPos().x < 0 && m_Enemy[i]->m_bActive == true)
		{
			m_Enemy[i]->Die();
			score++;
			sprintf(coinScore, "score: %d", score);
		}
	}
	if (gametime >= 0.7f && createcount < D_ENEMY_MAX)
	{
		srand(unsigned(time(NULL)));
		num = rand() % 2;
		num += 1;
		m_Enemy[createcount]->create(ccp(800, num * 150 + 50));
		createcount++;
		gametime = 0.0f;
	}
	if (createcount > 99)
	{
		createcount = 0;
	}
}

bool MiniScene::RectVsRectCollision(Rect rect1, Rect rect2)
{
	if (rect1.origin.x <= rect2.origin.x + rect2.size.width &&
		rect1.origin.x + rect1.size.width >= rect2.origin.x &&
		rect1.origin.y - rect1.size.height <= rect2.origin.y &&
		rect1.origin.y >= rect2.origin.y - rect2.size.height)
		return true;
	return false;
}
bool MiniScene::onTouchBegan(Touch* touch, Event* unused_event)
{
	Point location = touch->getLocation();
	m_ingame->pos(0);
	CCLog("touch");
	return true;

}
void MiniScene::onTouchMoved(Touch* touch, Event* unused_event)
{
	Point location = touch->getLocation();
	//m_ingame->pos(location);
	//int a, b;
	//a=location.x;
	//b = location.y;
	//CCLog("%d %d", a,b);
}
void MiniScene::onTouchCancelled(Touch* touch, Event* unused_event)
{

}
void MiniScene::onTouchEnded(Touch* touch, Event *unused_event)
{

}