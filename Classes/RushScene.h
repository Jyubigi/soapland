#pragma once

class RushScene : public AT::GameScene<RushScene>
{
private:
	int				move;
	int				stage;
	float collimin;
	float collimax;

	int m_nObCount;
	int m_nowcar;
	std::string m_strFile;
	int  m_pBoxes[5][5];
	Sprite * back; // 임시 이미지 
	std::vector<Aobject*> m_pObjects;

	Sprite * tempsprite;
	bool tempdirection;
	int tempsize;
	int tempnum;
	Vec2	m_cMovingIndex;

	Layer	*		m_pLayer;

public:
	RushScene();
	virtual ~RushScene();
	//!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
	virtual bool init();
	//!< 콜백 함수 (이 씬에선 종료할 때 쓰임)
	virtual void onExit();

	void Update(float dt);
	void end(int overcount);

	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onTouchMoved(Touch* touch, Event* unused_event);
	virtual void onTouchCancelled(Touch* touch, Event* unused_event);
	virtual void onTouchEnded(Touch* touch, Event *unused_event);

	void Began(int i);

	Point putPointInGrid(Point pos, int size, bool direc);
	Vec2 sindexFinding(Point pos, int size, bool direc);

	//!< 씬을 등록 시킴
	CREATE_FUNC(RushScene);
};