#pragma once

class ResultLayer : public Layer
{
private:
	std::string inkey;
	GH::CGHSprite * m_pBack;
	GH::CGHSprite * m_pResult;
	E_GAME_NUM m_eGame;

public:
	ResultLayer();
	~ResultLayer(){}

	virtual bool init();
	void Destroy();
	//!< 콜백 함수 (이 씬에선 종료할 때 쓰임)

	virtual bool onTouchBegan(Touch* touch, Event* unused_event);

	void Exit(Ref * pSander);
	void Again(Ref * pSander);

	//!< 씬을 등록 시킴
	CREATE_FUNC(ResultLayer);
};