#include "stdafx.h"

static int _nloadedSpriteCount = 0;
static int _nSpriteCount = 18;

title::title()
{
	auto BG = new GH::CGHSprite;
	BG->initWithFile("UI/main.png");
	BG->setPosition(ccp(D_DESIGN_WIDTH / 2, D_DESIGN_HEIGHT / 2));
	this->addChild(BG, 0);

	m_pLabel = LabelTTF::create();
	m_pLabel->initWithString("Now loading...0%", "Arial", 30);
	m_pLabel->setPosition(D_DESIGN_WIDTH/2,D_DESIGN_HEIGHT/9);
	this->addChild(m_pLabel, 5);

	auto cache = Director::getInstance()->getTextureCache();

	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("main.mp3");

	AC::MapStorage::getInstance()->init();

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("texture_ch0.plist", "texture_ch0.png");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("texture_ch1.plist", "texture_ch1.png");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("texture_ch2.plist", "texture_ch2.png");

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("UI/texture_logo.plist", "UI/texture_logo.png");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("jelly/texture_run.plist", "jelly/texture_run.png");

	//cache->addImageAsync("UI/school_Logo.PNG", CC_CALLBACK_1(title::loadingCallBack, this));

	cache->addImageAsync("UI/rushgame.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("UI/aromagame.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("UI/jellygame.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("UI/yokangame.png", CC_CALLBACK_1(title::loadingCallBack, this));

	cache->addImageAsync("UI/BG.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("UI/popup1.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("UI/option.png", CC_CALLBACK_1(title::loadingCallBack, this));

	cache->addImageAsync("image.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("ingame.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("ob2.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("ob3.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("ob4.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("ob5.png", CC_CALLBACK_1(title::loadingCallBack, this));

	cache->addImageAsync("left.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("right.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("Enemy.png", CC_CALLBACK_1(title::loadingCallBack, this));
	cache->addImageAsync("aroma/aroma.png", CC_CALLBACK_1(title::loadingCallBack, this));

	cache->addImageAsync("jelly/jelly.png", CC_CALLBACK_1(title::loadingCallBack, this));
}
bool title::init()
{
	if (!Layer::init())
		return false;

	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("title.mp3", true);

	schedule(schedule_selector(title::Update), 0.0f);
}
void title::loadingCallBack(Object *obj)
{
	_nloadedSpriteCount++;
	char tmp[256];
	sprintf(tmp, "Now loading...%d%%", (int)((float)_nloadedSpriteCount / (float)_nSpriteCount * 100));
	log("log : %s",tmp);
	log("log : LoadedSpriteCount = %d", _nloadedSpriteCount);
	m_pLabel->setString(tmp);
	if (_nloadedSpriteCount == _nSpriteCount)
	{
		m_pLabel->setString("Touch to start...");
		auto listener = EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);
		listener->onTouchBegan = CC_CALLBACK_2(title::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
}
void title::Update(float dt)
{

}
bool title::onTouchBegan(Touch* touch, Event* unused_event)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	this->removeChild(m_pLabel, true);
	CCScene* pScene = chgame::createScene();
	CCTransitionScene* pTran = CCTransitionFade::create(1.0f, pScene);
	CCDirector::sharedDirector()->replaceScene(pTran);
	return true;
}
