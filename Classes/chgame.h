#pragma once

class chgame : public LayerColor
{
private:
	Point		m_cPos;
	GH::CGHSprite	*		spr;
	AC::exitLayer * m_pExit = nullptr;

public:
	//!< 씬 생성에 대한 싱글톤
	static Scene* createScene();

	//!< 초기화 하는 구문
	//!< bool 형으로 만든 이유는 true 면 정상적인 반환
	//!< false 면 정상적으로 안되었기 때문에 오류 메시지를 반환하기 위함
	virtual bool init();
	//!< 콜백 함수 (이 씬에선 종료할 때 쓰임)

	virtual void Update(float dt);

	void game1(Ref* pSender);
	void game2(Ref* pSender);
	void game3(Ref* pSender);
	void game4(Ref* pSender);

	void optionCallBack(Ref* pSender);
	void createrCallBack(Ref* pSender);
	void storyCallBack(Ref* pSender);

	void exit();

	//!< 씬을 등록 시킴
	CREATE_FUNC(chgame);
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event);
};