#pragma once

namespace AC
{
	class MapStorage : public TempleteSingleton<MapStorage>
	{
	private:
		std::string m_pMaps[10];
	public:
		MapStorage(){}
		~MapStorage(){}
	public:
		void init();
		const char* getMapAt(int at);

	};
}