#include "stdafx.h"

/**
 @brief : 초기화 하는 구문
 @param : 없음
*/
bool HelloWorld::init()
{
	//!< 최상위 레이어에서 초기화를 시행하고 초기화가 정상적으로 되었는지 확인
	//!< 만약 false 를 하게 되면 정상적이지 않은걸로 판단하고 이하의 구문 실행 안함
	if ( !Layer::init() )
		return false;
	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}
	//!< 화면 사이즈 알아오는 구문
    Size visibleSize = Director::getInstance()->getVisibleSize();
	//!< 화면 중심 위치 알아오는 구문
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	count = 0.0f;
	m_bDestroyed = false;
	gm = new GH::CGHSprite;
	gm->initWithFile("UI/school_Logo.PNG");
	gm->setPosition(480 / 2,800 / 2);
	this->addChild(gm, 1);
	log("log : school logo init...");

	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("title.mp3");

	schedule(schedule_selector(HelloWorld::Update) , 0.1f);


    return true;
}

void HelloWorld::destroy()
{
	log("log : school logo destroy...");
	gm->removeFromParentAndCleanup(true);
	CC_SAFE_DELETE(gm);
	Scene *pScene = TransitionFade::create(1.0f, logo::createScene(),ccWHITE);
	Director::getInstance()->replaceScene(pScene);
}
void HelloWorld::Update(float dt)
{
	count += dt;
	if (count > 2.5f)
	{	

		if (!m_bDestroyed)
		{
			destroy();
			m_bDestroyed = true;
		}
	}
}