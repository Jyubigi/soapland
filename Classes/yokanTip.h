#pragma once

class YokanTip : public Layer
{
private:
	GH::CGHSprite * back[2];
	int touchcount;
public:
	virtual bool init();
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onEnterTransitionDidFinish()
	{
		auto listener = EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);

		listener->onTouchBegan = CC_CALLBACK_2(YokanTip::onTouchBegan, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}

	CREATE_SCENE_FUNC(YokanTip);
	CREATE_FUNC(YokanTip);
};