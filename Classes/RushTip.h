#pragma once

class RushTip : public Layer
{
private:
	int touchcount;
	GH::CGHSprite * back[2];
	bool m_bFinish = false;
public:
	virtual bool init();
	virtual bool onTouchBegan(Touch* touch, Event* unused_event);
	virtual void onEnterTransitionDidFinish()
	{
		auto listener = EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);

		listener->onTouchBegan = CC_CALLBACK_2(RushTip::onTouchBegan, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
	CREATE_SCENE_FUNC(RushTip);
	CREATE_FUNC(RushTip);
};